export default class Router {
	static #menuElement;
	static titleElement;
	static contentElement;
	/**
	 * Tableau des routes/pages de l'application.
	 * @example `Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }]`
	 */
	static routes = [];

	/**
	 * Affiche la page correspondant à `path` dans le tableau `routes`
	 * @param {String} path URL de la page à afficher
	 */
	static navigate(path) {
		const route = this.routes.find(route => route.path === path);
		if (route) {
			// affichage du titre de la page
			this.titleElement.innerHTML = `<h1>${route.title}</h1>`;
			// affichage de la page elle même
			this.contentElement.innerHTML = route.page.render();
			route.page.mount?.(this.contentElement);
			
		}
	}
	static set menuElement(element) { // setter
		this.#menuElement = element;
		const tab = element.querySelectorAll('a');
		
		tab.forEach(values => {
			values.addEventListener('click', event => {
				event.preventDefault();
				this.navigate(values.getAttribute('href'));
			});
		});
	}	
}
