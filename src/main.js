import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import { doc } from 'prettier';
import Component from './components/Component';
import PizzaForm from './PizzaForm';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList(data),
	aboutPage = new Component('p', null, 'ce site est génial'),
	pizzaForm = new PizzaForm();


Router.routes = [
    { path: '/', page: pizzaList, title: 'La carte' },
    { path: '/a-propos', page: aboutPage, title: 'À propos' },
    { path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];
    
Router.navigate('/'); // affiche la liste des pizzas

const logo = document.querySelector( ".logo" );
logo.innerHTML += `<small>Les pizzas c\'est la vie</small>`;
document.querySelector('.pizzaListLink').setAttribute('class', 'active');
document.querySelector('section').setAttribute('style', '');
var b = document.querySelector('.closeButton');

b.addEventListener('click', event => {
    event.preventDefault();
    document.querySelector('section').setAttribute('style', 'display:none');
})

Router.menuElement = document.querySelector('.mainMenu');
