import { format } from 'prettier';
import Page from './pages/Page.js';

export default class PizzaForm extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
        super.mount(element);
        const form = document.querySelector('form');
        const input = this.element.querySelector('input[name=name]');

        form.addEventListener('submit', event => { // détection de la soumission
            event.preventDefault(); // on empêche la page de se recharger
            if(input.value == '')
                alert("ya rien");
            else
                console.log('Le formulaire a été soumis avec la valeur :'+input.value);
                alert("la zappi a été ajouter fréro");
        });
        
	}

	submit(event) {}
}
